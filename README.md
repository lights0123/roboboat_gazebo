# RoboBoat Gazebo RoboBoat package
_SDF models and world of RoboBoat for Gazebo_

We want to define each task independently, so we can test ASV with given task. Later we want to combine them into a single world.

## Getting started

Dependencies:
* Gazebo 9 or 11 (untested)
* Python 3.8, and `pip install python-sdformat`
* [PX4/PX4-SITL_gazebo-classic: Set of plugins, models and worlds to use with OSRF Gazebo Simulator in SITL and HITL.](https://github.com/PX4/PX4-SITL_gazebo-classic)

For interacting with auto generation scripts, refer to [instructions in scripts folder](./scripts/README.md)

## Installation
Clone to catkin_ws and install. TODO guide

## Usage

How to run with minimal Gazebo install, outside docker.

```bash
# we need models from PX4-SITL_gazebo-classic, which is avaiable as a submodule in PX4-Autopilot
export GAZEBO_MODEL_PATH=$GAZEBO_MODEL_PATH:../../PX4-Autopilot/Tools/simulation/gazebo-classic/sitl_gazebo-classic/models
# and we point to our models
export GAZEBO_MODEL_PATH=$GAZEBO_MODEL_PATH:$(pwd)/models
gazebo --verbose worlds/course_a.world
# or gzserver
```

You can use `roslaunch` if you have installed this package
```bash
roslaunch roboboat_gazebo gazebo_ros.launch world:=course_a
```

Where `world:=` refers to world name with given task.

They sometimes might be open in fullscreen mode.

For <include> in .world files to work models need to be added to hidden folder ~/.gazebo/models

## Contributing

We are using SDF 1.7
Consult README files in each directory for specifics. 

### Adding models
Each model should have name as defined in https://outline.simle.pl/doc/mapping-simulation-xwtMWkXNhU
We want to create models for each object. So we can reuse them, or link them in world files. If we change anything within a model, we want it reflected in the world without changing much.

### Adding worlds
You can create them using Gazebo editor. But if it contains a lot of objects, generating them would be preffered, especially for task 2.

## Authors and acknowledgment
* Norbert Szulc
* Jakub Wilk
* Wiktoria Piech
* Piotr Stroiński

Thanks to ChatGPT for help creating some of the scripts

## License
GPL v3

## Project status
Development will stop after RoboBoat 2023, unless we will attempt next year competition.
